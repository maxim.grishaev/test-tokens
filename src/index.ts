export {
  TOKEN_TYPE,
  tokenAddressToSymbol,
  isValidToken,
  getDecimals,
  contractAddressToDex,
  getContractsByDexName,
  getAddressWithTokenType,
  getTokenFromContract,
  getTickerFromContract,
  getImageFromContract,
  getContractFromTicker,
  convertTokenToDecimals, // TODO better naming
  applyDecimalsToToken, // TODO better naming
  getAddressBasedOnTokenType,
} from "./utils";
import tokensConfig from "./tokens.json";

export const TOKENS = tokensConfig;
export { DEXES, CONTRACTS } from "./contracts";
