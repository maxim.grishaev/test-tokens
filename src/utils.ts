import BigNumber from 'bignumber.js';
import tokensConfig from './tokens.json';
import { DEXES, CONTRACTS } from './contracts';

// TODO do we need to export it anymore?
export const TEZ = 'xtz'; // TODO remove as obsolete ?? why???
const FA12 = 'FA12';
const FA2 = 'FA2';
const XTZ = 'XTZ';

export const TOKEN_TYPE = {
  XTZ,
  FA12,
  FA2,
};

// FIXME TODO where it;s uswed? Why not TOKEN_TYPE.XTZ ?
interface TokenRaw {
  type: 'string';
  tokenType: 'FA12' | 'FA2';
  id: 'string';
  fa2TokenId?: number;
  decimals: number;
  symbol: 'string';
  name: string;
  imgUrl: string;
}

class Token {
  private token: TokenRaw;
  constructor(token: TokenRaw) {
    this.token = token;
  }
  public getType() {
    return this.token.tokenType;
  }
  public getImageFromContract() {
    return this.token.imgUrl;
  }
  public getNormalizedAddress() {
    const { id: address, fa2TokenId: tokenId, tokenType } = this.token;

    return tokenType === TOKEN_TYPE.FA2 ? `${address}_${tokenId}` : address;
  }
}

export const tokens = new Map(
  tokensConfig.map(token => [
    token.tokenType === TOKEN_TYPE.FA12 ? token.id : `${token.id}_${token.id}`,
    new Token(token as TokenRaw),
  ])
);

/* [DEXES.PLENTY_CTEZ, CONTRACTS.PLENTY_CTEZ_CONTRACT_ADDRESSES], */
// TODO implement this to generate the contractAddressToDex array fro feeding the Map
// this and other fixes allows to have the DEXES as single soure of truth
const contractAddressName = (dex: string) => `${dex}_CONTRACT_ADDRESSES`;

const DEXESmap = Object.values(DEXES).map(dex => [
  dex,
  CONTRACTS[contractAddressName(dex)],
]);
/* ]).filter(([dex]) => [DEXES.PLENTY_CFMM].includes(dex)); */

export const getContractsByDexName = (dex: string) =>
  CONTRACTS[contractAddressName(dex)];

export const contractAddressToDex = new Map(
  [...DEXESmap].flatMap(([dex, addresses]) =>
    (addresses as string[]).map(address => [address, dex]) //TODO
  )
) as Map<string, keyof typeof DEXES>;

export const getDecimals = (tokenAddress: string) =>
  String(
    tokensConfig.find(el => {
      if (el.tokenType === TOKEN_TYPE.FA2) {
        const [address, id] = tokenAddress.split('_');
        return (
          el.id.toUpperCase() === address.toUpperCase() &&
          String(el.fa2TokenId) === id
        );
      } else {
        return el.id.toUpperCase() === tokenAddress.toUpperCase();
      }
    })?.decimals
  ); //TODO hand;e edgecases

/* "id": "KT193D4vozYnhGJQVtw7CoxxqphqUEEwK6Vb", */
/* "fa2TokenId": 0, */
// TODO FIXME - most likely duplicated somwhere

export const tokenAddressToSymbol = (address: string) => {
  if (address.toUpperCase() === XTZ) return XTZ;

  return tokensConfig.find(token => {
    const [inputAddress, fa2TokenId] = address.split('_');

    if (fa2TokenId) {
      return inputAddress === token.id && +fa2TokenId === token.fa2TokenId;
    } else {
      return inputAddress === token.id;
    }
  })?.symbol;
};

type TokenInput = {
  ({ tokenType }: { tokenType: typeof XTZ }): string;
  ({
    tokenType,
    address,
    tokenId,
  }: {
    tokenType: typeof FA2;
    address: string;
    tokenId: string;
  }): string;
  ({ tokenType, address }: { tokenType: typeof FA12; address: string }): string;
};

export const getAddressBasedOnTokenType: TokenInput = ({
  tokenType,
  address,
  tokenId,
}: {
  tokenType: typeof XTZ | typeof FA2 | typeof FA12;
  address?: any;
  tokenId?: any;
}) => {
  switch (tokenType) {
    case 'FA12': {
      return address;
    }
    case 'FA2': {
      return `${address}_${tokenId}`;
    }
    case 'XTZ':
      return TEZ;
  }
};

// TODO add text
export const getAddressWithTokenType = ({
  tokenType,
  address,
  tokenId,
}: {
  tokenType: string;
  address?: string;
  tokenId?: string;
}) => {
  if (tokenType === TOKEN_TYPE.XTZ) {
    return TEZ;
  }
  return tokenType === TOKEN_TYPE.FA2 ? `${address}_${tokenId}` : address;
};

export const isValidToken = (address: string) =>
  Boolean(tokensConfig.find(t => t.id === address));

export const getTokenFromContract = (contract_raw: string) => {
  const [contract, fa2TokenId] = contract_raw.split('_');
  return tokensConfig.find(token => {
    return (
      token.id.toUpperCase() === contract.toUpperCase() &&
      (fa2TokenId !== undefined ? token.fa2TokenId === +fa2TokenId : true)
    );
  });
};

export const getTickerFromContract = (contract_raw: string) => {
  return getTokenFromContract(contract_raw)?.symbol;
};

export const getImageFromContract = (contract_raw: string) => {
  return getTokenFromContract(contract_raw)?.imgUrl;
};

export const getContractFromTicker = (ticker: string) => {
  const token = tokensConfig.find(
    token => token.symbol.toUpperCase() === ticker.toUpperCase()
  );
  if (!token) return;
  return `${token.id}${
    token?.fa2TokenId !== undefined ? '_' + token.fa2TokenId : ''
  }`;
};

export const convertTokenToDecimals = (
  tokenAddress: string,
  amount: string
) => {
  const decimals = getTokenFromContract(tokenAddress)?.decimals;
  return decimals
    ? new BigNumber(amount).multipliedBy(
        new BigNumber(10).exponentiatedBy(decimals)
      )
    : undefined;
};

export const applyDecimalsToToken = (tokenAddress: string, amount: string) => {
  const decimals = getTokenFromContract(tokenAddress)?.decimals;
  return decimals
    ? new BigNumber(amount).div(
        new BigNumber(10).exponentiatedBy(decimals)
      )
    : undefined;
};
